/**
* Server side code.
*/
"use strict";
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT || 4000;
console.log(NODE_PORT);
app.use(express.static(__dirname + "/../client/"));

console.log("-----")

app.post("/submit-regForm", function (req, res) {
    console.log("Received form object " + req.body);
    console.log("Received form object " + JSON.stringify(req.body));
    
    return (1);
   // res.status(200).json(quiz);
 });




app.use(function (req, res) {
   res.send("<h1>!!!! Page not found ! ! !</h1>");
});

app.listen(NODE_PORT, function () {
   console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app
