(function () {
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);
    
    RegCtrl.$inject = ["$http"];

    function RegCtrl($http) {
        var regCtrlself  = this;
        
        regCtrlself.onSubmit = onSubmit;
        regCtrlself.initForm = initForm;
        regCtrlself.onlyFemale = onlyFemale;

        regCtrlself.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        regCtrlself.user = {
            
        }

        regCtrlself.nationalities = [
            { name: "Please select" , value:0},
            { name: "Singaporean", value: 1},
            { name: "Indonesian", value: 2},
            { name: "Thai", value: 3},
            { name: "Malaysian", value: 4},
            { name: "Australian", value: 5}      
        ];

        function initForm(){
            regCtrlself.user.selectedNationality = "0";
            regCtrlself.user.gender = "F";
        }

        function onSubmit(){
            console.log(regCtrlself.user.email);
            console.log(regCtrlself.user.password);
            console.log(regCtrlself.user.confirmpassword);
            console.log(regCtrlself.user.gender);
            console.log(regCtrlself.user.fullName);
            console.log(regCtrlself.user.newfullName);
            console.log(regCtrlself.user.dob);
            console.log(regCtrlself.user.address);
            console.log(regCtrlself.user.selectedNationality);
            console.log(regCtrlself.user.contactNumber);

            $http.post("/submit-regForm", regCtrlself.user)
            .then(function (result) {
                console.log(result);

            }).catch(function (e) {
                console.log(e);
            });
     
        }

        function onlyFemale(){
            console.log("only female");
            return regCtrlself.user.gender == "F";
        }

        regCtrlself.initForm();
    }
    
})();    