# README #

# STEPS #

#Setup Bitbucket#
* create respo
* Choose public or private repo access level
* create readme.md

#Prepare project structure#
* create folder 
* mkdir <project name>
* mkdir <project name>/server
* mkdir <project name>/client
* new file: /.gitignore
* new file: /.bowerrc

#Prepare Git#

* git init
* Create a git ignore file .gitignore e.g. node_modules
* git remote -v (check on the current directory which remote is configured to)
* To remove the git origin remote ( git remote remove origin)
* add remote origin , git remote add origin <http git url>. e.g. git remote add origin https://wwhale@bitbucket.org/wwhale/day5wa.git

* (To config)
* git init
* git add .
* git remote add origin https://wwhale@bitbucket.org/wwhale/day5wa.git

* (To update)
* git add .
* git commit -m "<message>"
* git push orign master

* git pull origin master
* git clone origin master



#Global Utility installation#

* (node.js)
* cd to the working folder 
* npm install -g nodemon
* npm init
* -- entry point
* -- path from bit bucket

* (express.js)
* cd to the working folder 
* npm install –save express
* npm install
* -- node_modules folder created

* (bower.js)
* .bowerrc file create
* -- "directory": "client/bower_components"
* npm install -g bower
* npm i bower –g
* bower init
* bower install bootstrap font-awesome angular --save

* (body-parser)
* npm install body-parser --save

* (heroku)
* install heroku
* heroku login
* heroku create
* git remote add heroku https://git.heroku.com/murmuring-cove-50493.git
* git remote -v
* git add .
* git commit –m “message”* 
* git push heroku master -u


#To run nodemon#
* cd to folder
* nodemon
* node "filename".js

#to test#
* http://localhost:4000 or https://murmuring-cove-50493.herokuapp.com/



This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact